package main

import (
  "fmt"
)

func Sqrt(x float64) float64 {
  if x == 1.0 {
    return x
  }

  z := 1.0

  for attempts := 1; attempts < 10; attempts++ {
    if z * z == x {
      return z
    }

    square := z * z

    if square > x {
      z -= (square - x) / (2.0 * z)
    } else {
      z += (x - square) / (2.0 * z)
    }
  }

  return z
}

func main() {
  fmt.Println(Sqrt(1.5625))
  fmt.Println(Sqrt(2))
  fmt.Println(Sqrt(4))
  fmt.Println(Sqrt(9))
  fmt.Println(Sqrt(25))
}
